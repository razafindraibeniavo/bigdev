$(document).ready(function () {
    /*Player Table*/
    if ($('#play').length) {
        var playerTable = $("#play").DataTable({
            ajax: {
                url: '/api/players?order[id]=asc',
            },
            columns: [
                {data: "name"},
                {data: "birthDate"},
                {data: "nationality"},
                {data: "goalsNumber"},
                {data: "clubTeam.name"},
                {data: "nationalTeam.name"},
                {
                    data: "action",
                    render: function (data, type, row) {
                        return `
                        <a href="/player/${row.id}" class="btn btn-primary">Voir</a>
                        <a href="/player/${row.id}/edit" class="btn btn-success">Update</a>
                        <button class="btn btn-danger delete-btn" data-id="${row.id}">Supprimer</button>
                    `;
                    }
                },
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            }
        });
    }
    /*Club Table*/
    if ($('#club').length) {
        var clubTable = $("#club").DataTable({
            ajax: {
                url: '/api/club_teams?order[id]=asc',
            },
            columns: [
                {data: "name"},
                {
                    data: "action",
                    render: function (data, type, row) {
                        return `
                        <a href="/club/team/${row.id}" class="btn btn-primary">Voir</a>
                        <a href="/club/team/${row.id}/edit" class="btn btn-success">Update</a>
                        <button class="btn btn-danger delete-btn" data-id="${row.id}">Supprimer</button>
                    `;
                    }
                },
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            }
        });
    }
    /*National Table*/
    if ($('#national').length) {
        var nationalTable = $("#national").DataTable({
            ajax: {
                url: '/api/national_teams?order[id]=asc',
            },
            columns: [
                {data: "name"},
                {
                    data: "action",
                    render: function (data, type, row) {
                        return `
                        <a href="/national/team/${row.id}" class="btn btn-primary">Voir</a>
                        <a href="/national/team/${row.id}/edit" class="btn btn-success">Update</a>
                        <button class="btn btn-danger delete-btn" data-id="${row.id}">Supprimer</button>
                    `;
                    }
                },
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            }
        });
    }

    $('#play').on('click', '.delete-btn', function () {
        var playerId = $(this).data('id');
        if (window.confirm("Souhaitez-vous vraiment le supprimer  ?")) {
            $.ajax({
                url: '/api/players/' + playerId,
                type: 'DELETE',
                success: function (result) {
                    playerTable.ajax.reload();
                    alert('Suppression réussie');
                    // Mettre à jour la DataTables ou effectuer d'autres actions après la suppression
                },
                error: function (xhr, status, error) {
                    console.error('Erreur lors de la suppression :', status, error);
                    // Gérer l'erreur ici
                }
            });
        }
    });

    $('#club').on('click', '.delete-btn', function () {
        var clubId = $(this).data('id');
        if (window.confirm("Souhaitez-vous vraiment le supprimer  ?")) {
            $.ajax({
                url: '/api/club_teams/' + clubId,
                type: 'DELETE',
                success: function (result) {
                    clubTable.ajax.reload();
                    alert('Suppression réussie');
                    // Mettre à jour la DataTables ou effectuer d'autres actions après la suppression
                },
                error: function (xhr, status, error) {
                    console.error('Erreur lors de la suppression :', status, error);
                    // Gérer l'erreur ici
                }
            });
        }
    });
    $('#national').on('click', '.delete-btn', function () {
        var nationalId = $(this).data('id');
        if (window.confirm("Souhaitez-vous vraiment le supprimer  ?")) {
            $.ajax({
                url: '/api/national_teams/' + nationalId,
                type: 'DELETE',
                success: function (result) {
                    nationalTable.ajax.reload();
                    alert('Suppression réussie');
                    // Mettre à jour la DataTables ou effectuer d'autres actions après la suppression
                },
                error: function (xhr, status, error) {
                    console.error('Erreur lors de la suppression :', status, error);
                    // Gérer l'erreur ici
                }
            });
        }
    });
});