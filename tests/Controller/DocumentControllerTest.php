<?php

namespace App\Test\Controller;

use App\Entity\Document;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DocumentControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private EntityManagerInterface $manager;
    private EntityRepository $repository;
    private string $path = '/document/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = static::getContainer()->get('doctrine')->getManager();
        $this->repository = $this->manager->getRepository(Document::class);

        foreach ($this->repository->findAll() as $object) {
            $this->manager->remove($object);
        }

        $this->manager->flush();
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Document index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $this->markTestIncomplete();
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'document[compteAffaire]' => 'Testing',
            'document[compteEvent]' => 'Testing',
            'document[compteDerEv]' => 'Testing',
            'document[numFiche]' => 'Testing',
            'document[libelleCiv]' => 'Testing',
            'document[proprioVehicule]' => 'Testing',
            'document[nom]' => 'Testing',
            'document[prenom]' => 'Testing',
            'document[numVoie]' => 'Testing',
            'document[compAdresse]' => 'Testing',
            'document[codePostal]' => 'Testing',
            'document[ville]' => 'Testing',
            'document[telDom]' => 'Testing',
            'document[telPor]' => 'Testing',
            'document[telJob]' => 'Testing',
            'document[email]' => 'Testing',
            'document[dateCirculation]' => 'Testing',
            'document[dateAchat]' => 'Testing',
            'document[dateDerEnv]' => 'Testing',
            'document[libelleMarque]' => 'Testing',
            'document[libelleModele]' => 'Testing',
            'document[version]' => 'Testing',
            'document[vin]' => 'Testing',
            'document[immatriculation]' => 'Testing',
            'document[typeProspect]' => 'Testing',
            'document[kilometrage]' => 'Testing',
            'document[energie]' => 'Testing',
            'document[vendeurVn]' => 'Testing',
            'document[vendeurVO]' => 'Testing',
            'document[commentaireFacture]' => 'Testing',
            'document[typeVnVo]' => 'Testing',
            'document[numDVnVo]' => 'Testing',
            'document[interVen]' => 'Testing',
            'document[DateEv]' => 'Testing',
            'document[originEnv]' => 'Testing',
        ]);

        self::assertResponseRedirects($this->path);

        self::assertSame(1, $this->repository->count([]));
    }

    public function testShow(): void
    {
        $this->markTestIncomplete();
        $fixture = new Document();
        $fixture->setCompteAffaire('My Title');
        $fixture->setCompteEvent('My Title');
        $fixture->setCompteDerEv('My Title');
        $fixture->setNumFiche('My Title');
        $fixture->setLibelleCiv('My Title');
        $fixture->setProprioVehicule('My Title');
        $fixture->setNom('My Title');
        $fixture->setPrenom('My Title');
        $fixture->setNumVoie('My Title');
        $fixture->setCompAdresse('My Title');
        $fixture->setCodePostal('My Title');
        $fixture->setVille('My Title');
        $fixture->setTelDom('My Title');
        $fixture->setTelPor('My Title');
        $fixture->setTelJob('My Title');
        $fixture->setEmail('My Title');
        $fixture->setDateCirculation('My Title');
        $fixture->setDateAchat('My Title');
        $fixture->setDateDerEnv('My Title');
        $fixture->setLibelleMarque('My Title');
        $fixture->setLibelleModele('My Title');
        $fixture->setVersion('My Title');
        $fixture->setVin('My Title');
        $fixture->setImmatriculation('My Title');
        $fixture->setTypeProspect('My Title');
        $fixture->setKilometrage('My Title');
        $fixture->setEnergie('My Title');
        $fixture->setVendeurVn('My Title');
        $fixture->setVendeurVO('My Title');
        $fixture->setCommentaireFacture('My Title');
        $fixture->setTypeVnVo('My Title');
        $fixture->setNumDVnVo('My Title');
        $fixture->setInterVen('My Title');
        $fixture->setDateEv('My Title');
        $fixture->setOriginEnv('My Title');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Document');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $this->markTestIncomplete();
        $fixture = new Document();
        $fixture->setCompteAffaire('Value');
        $fixture->setCompteEvent('Value');
        $fixture->setCompteDerEv('Value');
        $fixture->setNumFiche('Value');
        $fixture->setLibelleCiv('Value');
        $fixture->setProprioVehicule('Value');
        $fixture->setNom('Value');
        $fixture->setPrenom('Value');
        $fixture->setNumVoie('Value');
        $fixture->setCompAdresse('Value');
        $fixture->setCodePostal('Value');
        $fixture->setVille('Value');
        $fixture->setTelDom('Value');
        $fixture->setTelPor('Value');
        $fixture->setTelJob('Value');
        $fixture->setEmail('Value');
        $fixture->setDateCirculation('Value');
        $fixture->setDateAchat('Value');
        $fixture->setDateDerEnv('Value');
        $fixture->setLibelleMarque('Value');
        $fixture->setLibelleModele('Value');
        $fixture->setVersion('Value');
        $fixture->setVin('Value');
        $fixture->setImmatriculation('Value');
        $fixture->setTypeProspect('Value');
        $fixture->setKilometrage('Value');
        $fixture->setEnergie('Value');
        $fixture->setVendeurVn('Value');
        $fixture->setVendeurVO('Value');
        $fixture->setCommentaireFacture('Value');
        $fixture->setTypeVnVo('Value');
        $fixture->setNumDVnVo('Value');
        $fixture->setInterVen('Value');
        $fixture->setDateEv('Value');
        $fixture->setOriginEnv('Value');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'document[compteAffaire]' => 'Something New',
            'document[compteEvent]' => 'Something New',
            'document[compteDerEv]' => 'Something New',
            'document[numFiche]' => 'Something New',
            'document[libelleCiv]' => 'Something New',
            'document[proprioVehicule]' => 'Something New',
            'document[nom]' => 'Something New',
            'document[prenom]' => 'Something New',
            'document[numVoie]' => 'Something New',
            'document[compAdresse]' => 'Something New',
            'document[codePostal]' => 'Something New',
            'document[ville]' => 'Something New',
            'document[telDom]' => 'Something New',
            'document[telPor]' => 'Something New',
            'document[telJob]' => 'Something New',
            'document[email]' => 'Something New',
            'document[dateCirculation]' => 'Something New',
            'document[dateAchat]' => 'Something New',
            'document[dateDerEnv]' => 'Something New',
            'document[libelleMarque]' => 'Something New',
            'document[libelleModele]' => 'Something New',
            'document[version]' => 'Something New',
            'document[vin]' => 'Something New',
            'document[immatriculation]' => 'Something New',
            'document[typeProspect]' => 'Something New',
            'document[kilometrage]' => 'Something New',
            'document[energie]' => 'Something New',
            'document[vendeurVn]' => 'Something New',
            'document[vendeurVO]' => 'Something New',
            'document[commentaireFacture]' => 'Something New',
            'document[typeVnVo]' => 'Something New',
            'document[numDVnVo]' => 'Something New',
            'document[interVen]' => 'Something New',
            'document[DateEv]' => 'Something New',
            'document[originEnv]' => 'Something New',
        ]);

        self::assertResponseRedirects('/document/');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getCompteAffaire());
        self::assertSame('Something New', $fixture[0]->getCompteEvent());
        self::assertSame('Something New', $fixture[0]->getCompteDerEv());
        self::assertSame('Something New', $fixture[0]->getNumFiche());
        self::assertSame('Something New', $fixture[0]->getLibelleCiv());
        self::assertSame('Something New', $fixture[0]->getProprioVehicule());
        self::assertSame('Something New', $fixture[0]->getNom());
        self::assertSame('Something New', $fixture[0]->getPrenom());
        self::assertSame('Something New', $fixture[0]->getNumVoie());
        self::assertSame('Something New', $fixture[0]->getCompAdresse());
        self::assertSame('Something New', $fixture[0]->getCodePostal());
        self::assertSame('Something New', $fixture[0]->getVille());
        self::assertSame('Something New', $fixture[0]->getTelDom());
        self::assertSame('Something New', $fixture[0]->getTelPor());
        self::assertSame('Something New', $fixture[0]->getTelJob());
        self::assertSame('Something New', $fixture[0]->getEmail());
        self::assertSame('Something New', $fixture[0]->getDateCirculation());
        self::assertSame('Something New', $fixture[0]->getDateAchat());
        self::assertSame('Something New', $fixture[0]->getDateDerEnv());
        self::assertSame('Something New', $fixture[0]->getLibelleMarque());
        self::assertSame('Something New', $fixture[0]->getLibelleModele());
        self::assertSame('Something New', $fixture[0]->getVersion());
        self::assertSame('Something New', $fixture[0]->getVin());
        self::assertSame('Something New', $fixture[0]->getImmatriculation());
        self::assertSame('Something New', $fixture[0]->getTypeProspect());
        self::assertSame('Something New', $fixture[0]->getKilometrage());
        self::assertSame('Something New', $fixture[0]->getEnergie());
        self::assertSame('Something New', $fixture[0]->getVendeurVn());
        self::assertSame('Something New', $fixture[0]->getVendeurVO());
        self::assertSame('Something New', $fixture[0]->getCommentaireFacture());
        self::assertSame('Something New', $fixture[0]->getTypeVnVo());
        self::assertSame('Something New', $fixture[0]->getNumDVnVo());
        self::assertSame('Something New', $fixture[0]->getInterVen());
        self::assertSame('Something New', $fixture[0]->getDateEv());
        self::assertSame('Something New', $fixture[0]->getOriginEnv());
    }

    public function testRemove(): void
    {
        $this->markTestIncomplete();
        $fixture = new Document();
        $fixture->setCompteAffaire('Value');
        $fixture->setCompteEvent('Value');
        $fixture->setCompteDerEv('Value');
        $fixture->setNumFiche('Value');
        $fixture->setLibelleCiv('Value');
        $fixture->setProprioVehicule('Value');
        $fixture->setNom('Value');
        $fixture->setPrenom('Value');
        $fixture->setNumVoie('Value');
        $fixture->setCompAdresse('Value');
        $fixture->setCodePostal('Value');
        $fixture->setVille('Value');
        $fixture->setTelDom('Value');
        $fixture->setTelPor('Value');
        $fixture->setTelJob('Value');
        $fixture->setEmail('Value');
        $fixture->setDateCirculation('Value');
        $fixture->setDateAchat('Value');
        $fixture->setDateDerEnv('Value');
        $fixture->setLibelleMarque('Value');
        $fixture->setLibelleModele('Value');
        $fixture->setVersion('Value');
        $fixture->setVin('Value');
        $fixture->setImmatriculation('Value');
        $fixture->setTypeProspect('Value');
        $fixture->setKilometrage('Value');
        $fixture->setEnergie('Value');
        $fixture->setVendeurVn('Value');
        $fixture->setVendeurVO('Value');
        $fixture->setCommentaireFacture('Value');
        $fixture->setTypeVnVo('Value');
        $fixture->setNumDVnVo('Value');
        $fixture->setInterVen('Value');
        $fixture->setDateEv('Value');
        $fixture->setOriginEnv('Value');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertResponseRedirects('/document/');
        self::assertSame(0, $this->repository->count([]));
    }
}
