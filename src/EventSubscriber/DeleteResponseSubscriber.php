<?php

namespace App\EventSubscriber;

use ApiPlatform\Symfony\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class DeleteResponseSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['delete', EventPriorities::POST_WRITE],
        ];
    }

    public function delete(ViewEvent $event){
        $request = $event->getRequest();

        if (
            !$request->isMethod('DELETE')/* ||
            !($attributes = RequestAttributesExtractor::extractAttributes($request))*/
        ) {
            return;
        }


//        if (!($data instanceof ArticleDTO)) {
//            return;
//        }
        $entity = $event->getControllerResult();
        $event->setResponse(new JsonResponse([
            "status" => "success",
            "message" => "Suppression avec succès."
        ] , Response::HTTP_OK));
    }
}