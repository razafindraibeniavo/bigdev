<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Repository\PlayerRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: PlayerRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des joueurs",
            ],
            normalizationContext: ['groups' => 'player:read'],
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un joueur",
            ],
            normalizationContext: ['groups' => 'player:read'],
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un joueur",
            ],
            normalizationContext: ['groups' => 'player:read'],
            denormalizationContext: ['groups' => 'player:create'],
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un joueur",
            ],
            normalizationContext: ['groups' => 'player:read'],
            denormalizationContext: ['groups' => 'player:update'],
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un joueur",
            ],
        ),
    ],
    paginationEnabled: false
)]
class Player
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'player:create','player:update','player:read',
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'player:create','player:update','player:read',
    ])]
    private ?string $name = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'player:create','player:update','player:read',
    ])]
    private ?\DateTimeInterface $birthDate = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'player:create','player:update','player:read',
    ])]
    private ?string $nationality = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'player:create','player:update','player:read',
    ])]
    private ?int $goalsNumber = null;

    #[ORM\ManyToOne(inversedBy: 'players')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'player:create','player:update','player:read',
    ])]
    private ?ClubTeam $clubTeam = null;

    #[ORM\ManyToOne(inversedBy: 'players')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'player:create','player:update','player:read',
    ])]
    private ?NationalTeam $nationalTeam = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(?\DateTimeInterface $birthDate): static
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getNationality(): ?string
    {
        return $this->nationality;
    }

    public function setNationality(?string $nationality): static
    {
        $this->nationality = $nationality;

        return $this;
    }

    public function getGoalsNumber(): ?int
    {
        return $this->goalsNumber;
    }

    public function setGoalsNumber(?int $goalsNumber): static
    {
        $this->goalsNumber = $goalsNumber;

        return $this;
    }

    public function getClubTeam(): ?ClubTeam
    {
        return $this->clubTeam;
    }

    public function setClubTeam(?ClubTeam $clubTeam): static
    {
        $this->clubTeam = $clubTeam;

        return $this;
    }

    public function getNationalTeam(): ?NationalTeam
    {
        return $this->nationalTeam;
    }

    public function setNationalTeam(?NationalTeam $nationalTeam): static
    {
        $this->nationalTeam = $nationalTeam;

        return $this;
    }
}
