<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Repository\ClubTeamRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ClubTeamRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des clubs",
            ],
            normalizationContext: ['groups' => 'club_team:read'],
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un club",
            ],
            normalizationContext: ['groups' => 'club_team:read'],
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un club",
            ],
            normalizationContext: ['groups' => 'club_team:read'],
            denormalizationContext: ['groups' => 'club_team:create'],
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un club",
            ],
            normalizationContext: ['groups' => 'club_team:read'],
            denormalizationContext: ['groups' => 'club_team:update'],
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un club",
            ],
        ),
    ],
    paginationEnabled: false
)]
#[ApiFilter(
    OrderFilter::class,properties: [
    "id",
],arguments:  ['orderParameterName' => 'order'])
]
class ClubTeam
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'player:create','player:update','player:read',
        'club_team:create','club_team:update','club_team:read',
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(groups: [
        'player:create','player:update','player:read',
        'club_team:create','club_team:update','club_team:read',
    ])]
    private ?string $name = null;

    #[ORM\OneToMany(targetEntity: Player::class, mappedBy: 'clubTeam',cascade: ['persist','detach'])]
    private Collection $players;

    public function __construct()
    {
        $this->players = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Player>
     */
    public function getPlayers(): Collection
    {
        return $this->players;
    }

    public function addPlayer(Player $player): static
    {
        if (!$this->players->contains($player)) {
            $this->players->add($player);
            $player->setClubTeam($this);
        }

        return $this;
    }

    public function removePlayer(Player $player): static
    {
        if ($this->players->removeElement($player)) {
            // set the owning side to null (unless already changed)
            if ($player->getClubTeam() === $this) {
                $player->setClubTeam(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->name;
        // TODO: Implement __toString() method.
    }
}
