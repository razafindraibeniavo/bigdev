<?php

namespace App\Controller;

use App\Entity\NationalTeam;
use App\Form\NationalTeamType;
use App\Repository\NationalTeamRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/national/team')]
class NationalTeamController extends AbstractController
{
    #[Route('/', name: 'app_national_team_index', methods: ['GET'])]
    public function index(NationalTeamRepository $nationalTeamRepository): Response
    {
        return $this->render('national_team/index.html.twig', [
            'national_teams' => $nationalTeamRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_national_team_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $nationalTeam = new NationalTeam();
        $form = $this->createForm(NationalTeamType::class, $nationalTeam);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($nationalTeam);
            $entityManager->flush();

            return $this->redirectToRoute('app_national_team_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('national_team/new.html.twig', [
            'national_team' => $nationalTeam,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_national_team_show', methods: ['GET'])]
    public function show(NationalTeam $nationalTeam): Response
    {
        return $this->render('national_team/show.html.twig', [
            'national_team' => $nationalTeam,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_national_team_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, NationalTeam $nationalTeam, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(NationalTeamType::class, $nationalTeam);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_national_team_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('national_team/edit.html.twig', [
            'national_team' => $nationalTeam,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_national_team_delete', methods: ['POST'])]
    public function delete(Request $request, NationalTeam $nationalTeam, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$nationalTeam->getId(), $request->request->get('_token'))) {
            $entityManager->remove($nationalTeam);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_national_team_index', [], Response::HTTP_SEE_OTHER);
    }
}
