<?php

namespace App\Controller;

use App\Entity\ClubTeam;
use App\Form\ClubTeamType;
use App\Repository\ClubTeamRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/club/team')]
class ClubTeamController extends AbstractController
{
    #[Route('/', name: 'app_club_team_index', methods: ['GET'])]
    public function index(ClubTeamRepository $clubTeamRepository): Response
    {
        return $this->render('club_team/index.html.twig', [
            'club_teams' => $clubTeamRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_club_team_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $clubTeam = new ClubTeam();
        $form = $this->createForm(ClubTeamType::class, $clubTeam);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($clubTeam);
            $entityManager->flush();

            return $this->redirectToRoute('app_club_team_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('club_team/new.html.twig', [
            'club_team' => $clubTeam,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_club_team_show', methods: ['GET'])]
    public function show(ClubTeam $clubTeam): Response
    {
        return $this->render('club_team/show.html.twig', [
            'club_team' => $clubTeam,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_club_team_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ClubTeam $clubTeam, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ClubTeamType::class, $clubTeam);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_club_team_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('club_team/edit.html.twig', [
            'club_team' => $clubTeam,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_club_team_delete', methods: ['POST'])]
    public function delete(Request $request, ClubTeam $clubTeam, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$clubTeam->getId(), $request->request->get('_token'))) {
            $entityManager->remove($clubTeam);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_club_team_index', [], Response::HTTP_SEE_OTHER);
    }
}
